(ns oauthrepl.cache
  (:import [java.time Instant]))

(defn make-cache []
  (atom {}))

(defn cache-response [cache response]
  (swap! cache assoc :refresh-token (get response "refresh_token"))
  (swap! cache assoc :access-token (get response "access_token"))

  (swap! cache assoc :access-token-expiration
         (.plusSeconds (Instant/now) (get response "expires_in"))))


(defn expire-token! [cache]
  (let [access-token (:access-token @cache)
        access-token-expiration (:access-token-expiration @cache)]

    (when (not (and access-token
                    access-token-expiration
                    (.isBefore (Instant/now) access-token-expiration)))
      (swap! cache dissoc :access-token :access-token-expiration))))
