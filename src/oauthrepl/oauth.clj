(ns oauthrepl.oauth
  (:require [clj-http.client :as http]
            [clojure.data.json :as json]))

(defn redirect-uri [config]
  (format "http://localhost:%s/cb"
          (or (:port config) 3030)))

(defn- json-body [response]
  (json/read-str (:body response)))

(defn invoke-exchange-code [config code]
  (let [response (http/post (:token-uri config)
                            {:basic-auth [(:client-id config)
                                          (:client-secret config)]
                             :form-params
                             {:grant_type "authorization_code"
                              :code code
                              :redirect_uri (redirect-uri config)}})]
    (json-body response)))

(defn invoke-refresh-token [config refresh-token]
  (let [response (http/post (:token-uri config)

                            {:basic-auth [(:client-id config)
                                          (:client-secret config)]
                             :form-params
                             {:grant_type "refresh_token"
                              :refresh_token refresh-token
                              :redirect_uri (redirect-uri config)}})]
    (json-body response)))
