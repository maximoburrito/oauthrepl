(ns oauthrepl.server
  (:require [clj-http.client :as http]
            [clojure.core.async :as async]
            [clojure.java.browse :as browse]
            [clojure.string :as string]
            [oauthrepl.cache :as cache]
            [oauthrepl.notify :as notify]
            [oauthrepl.oauth :as oauth]
            [ring.adapter.jetty :as jetty]
            [ring.util.codec]
            [ring.util.response :as response])
  (:import [java.util UUID]))

(defn- gen-state []
  (str (UUID/randomUUID)))

(defn- make-init-uri [config state]
  (format "%s?%s"
          (:authorize-uri config)
          (http/generate-query-string
           {:response_type "code"
            :redirect_uri (oauth/redirect-uri config)
            :client_id (:client-id config)
            :scope (string/join " " (:scopes config))
            :state state})))

(defn- make-handler [config cache channel]
  (let [oauth2-state (gen-state)]
    (fn [request]
      (let [uri (:uri request)]
        (println "HTTP" (:request-method request) uri (:query-string request))
        (cond
          (= "/" uri)
          (response/redirect (make-init-uri config oauth2-state))

          (= "/exit" uri)
          (do
            (async/put! channel :exit)
            (response/response "So long, and thanks for all the tokens!"))

          (.startsWith uri "/cb")
          (let [params (ring.util.codec/form-decode (:query-string request))
                code (get params "code")
                state (get params "state")]
            (if (= state oauth2-state)
              (let [exchange-response (oauth/invoke-exchange-code config code)]
                (cache/cache-response cache exchange-response)
                (async/put! channel :ok)
                (response/response "Goodbye, and good luck!"))
              (response/response "Denied...")))
          :else
          (response/not-found "unknown URL"))))))


(defn do-oauth-server [config cache]
  (let [channel   (async/chan)
        timeout-s (or (:timeout config) 180)
        timeout   (async/timeout (* timeout-s 1000))
        port      (or (:port config) 3030)
        server    (jetty/run-jetty (make-handler config cache channel)
                                   {:port port
                                    :join? false})]
    (notify/notify config "Starting OAuth Web Server")
    (when (:open-browser config)
      (browse/browse-url (format "http://localhost:%s/" port)))
    (let [[result ch] (async/alts!! [channel timeout])]
      (if (= ch timeout)
        (notify/notify config "OAuth server timed out")
        (notify/notify config "Stopping OAuth Web Server"))
      (.stop server))))

