(ns oauthrepl.core
  (:require [oauthrepl.cache :as state]
            [oauthrepl.oauth :as oauth]
            [oauthrepl.server :as server]))

(defn- refresh-token! [config cache]
  (when-let [token (:refresh-token @cache)]
    (state/cache-response cache
                          (oauth/invoke-refresh-token config token))))

(defn get-token [config cache]
  (state/expire-token! cache)
  (when (not (:access-token @cache))
    (refresh-token! config cache))
  (when (not (:access-token @cache))
    (server/do-oauth-server config cache))
  (or (:access-token @cache)
      (throw (ex-info "unable to authenticate" {}))))

(defn token-manager [config]
  (let [cache (state/make-cache)]
    (fn []
      (get-token config cache))))
