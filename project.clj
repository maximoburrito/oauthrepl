(defproject oauthrepl "0.1.0-SNAPSHOT"
  :description "tools for doing oauth exploration from the repl"
  :url "http://example.com/FIXME"
  :license {:name "EPL-2.0 OR GPL-2.0-or-later WITH Classpath-exception-2.0"
            :url "https://www.eclipse.org/legal/epl-2.0/"}
  :dependencies [[org.clojure/clojure "1.10.0"]
                 [org.clojure/core.async "0.4.500"]
                 [org.clojure/data.json "0.2.6"]
                 [clj-http "3.10.0"]
                 [ring "1.7.1"]]
  :repl-options {:init-ns oauthrepl.core})
