(ns oauthrepl.notify
  (:require [clojure.java.shell :as shell]))

(defn notify [config message]
  (when (:notify config)
    (shell/sh "notify-send" "-t" "3000" "--" message)))
