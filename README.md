# oauthrepl

Manages OAuth tokens for use at the REPL.

## About

This code is derived from something I started work on at a hack day at
the Austin Clojure Meetup. I don't handle a number of OAuth error
cases, and the code is a bit rough. There are no tests, and the
notifications are linux-only. However, I've used it productively at the REPL for
poking at OAuth servers.

## Docs

Hey now, let's not get crazy.

## License

This program and the accompanying materials are made available under the
terms of the Eclipse Public License 2.0 which is available at
http://www.eclipse.org/legal/epl-2.0.a

This Source Code may also be made available under the following Secondary
Licenses when the conditions for such availability set forth in the Eclipse
Public License, v. 2.0 are satisfied: GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or (at your
option) any later version, with the GNU Classpath Exception which is available
at https://www.gnu.org/software/classpath/license.html.
